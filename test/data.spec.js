require('../src/data.js');

describe('sortData', () => {
  it('is a function', () => {
    expect(typeof app.sortData).toBe('function');
  })
  it('sorts in ascending order', () => {
    expect(app.sortData([{name: "Aa"}, {name: "Xx"}, {name: "Bb"}], "name", "ASC")).toEqual([{name: "Aa"}, {name: "Bb"}, {name: "Xx"}]);
  })
  it('sorts in descending order', () => {
    expect(app.sortData([{name: "Aa"}, {name: "Xx"}, {name: "Bb"}], "name", "DESC")).toEqual([{name: "Xx"}, {name: "Bb"}, {name: "Aa"}]);
  })
});

describe('filterData', () => {
  it('filters type', () => {
    expect(app.filterData([{name: "Aa", type: ["Grass"]}, {name: "Xx", type: ["Poison"]}, {name: "Bb", type: ["Water"]}], "Grass")).toEqual([{name: "Aa", type: ["Grass"]}]);
  })
  it('filters with two type', () => {
    expect(app.filterData([{name: "Aa", type: ["Grass", "Poison"]}, {name: "Xx", type: ["Poison", "Ice"]}, {name: "Bb", type: ["Water", "Ground"]}], "Poison")).toEqual([{name: "Aa", type: ["Grass", "Poison"]}, {name: "Xx", type: ["Poison", "Ice"]}]);
  })
});