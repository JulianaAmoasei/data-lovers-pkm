const data = getPokemonList()
const menuFiltro = document.getElementById("filtro-tipos")
const menuOrdem = document.getElementById("menu-ordem")
const mostraPokemonDiv = document.getElementById("mostra-pokemon")
const mostraAlturasDiv = document.getElementById("mostra-alturas")

function getPokemonList(){
    return POKEMON.pokemon.slice()
}

menuFiltro.addEventListener("change", () => selecionados(app.filterData(data, menuFiltro.value)))
menuOrdem.addEventListener("change", () => selecionados(app.sortData(data, menuOrdem.value, menuOrdem.options[menuOrdem.selectedIndex].dataset.ordem)))
document.getElementById("btn-alturas").addEventListener("click", () => exibeAlturas(app.calculaAlturas(data)))

window.onload = () => {
    carregaMenuTipos(data)
}

function carregaMenuTipos(data){
    const pokeTypes = []
    data.map(poke => poke.type.map(type => {
        if (!pokeTypes.includes(type)){
            pokeTypes.push(type)
        } else {
            return false
        }
    }))
    
    menuFiltro.innerHTML = ""
    menuFiltro.innerHTML = `<option value="none">Selecione filtro</option>`
    menuFiltro.innerHTML += pokeTypes.map(type => `<option value="${type}">${type}</option>`).join("")
}


function selecionados(obj){
    mostraAlturasDiv.innerHTML = ""
    mostraPokemonDiv.innerHTML = ""
    obj.forEach(poke => {
        mostraPokemonDiv.innerHTML += `
            <div class="card">
                <h4>${poke.name}</h4>
                <img src="${poke.img}" />
                <p>Tipo: ${poke.type.map(type => `${type}`).join(", ")}</p>
            </div>
        `
    })
}

function exibeAlturas({number: alturaMedia, array: arrayAlturas}){
    mostraPokemonDiv.innerHTML = ""
    const mostraAlturas = document.getElementById("mostra-alturas")
    arrayAlturas.forEach(poke => {
        mostraAlturas.innerHTML += `
        <img src="${poke.imagem}" style="width:${(poke.altura/alturaMedia)*10}%;">
        `
    })
}
