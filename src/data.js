function sortData(data, sortBy, sortOrder){
  if (sortOrder === "ASC"){
    sorted = data.sort((a, b) => (a[sortBy] > b[sortBy]) ? 1 : -1)
  }
  if (sortOrder === "DESC"){
    sorted = data.sort((a, b) => (a[sortBy] < b[sortBy]) ? 1 : -1)
  }
  return sorted
}

function filterData(data, condition){
  return data.filter(poke => poke.type.includes(condition))
}

function calculaAlturas(data){
  const alturas = data.map(poke => ({
    "pokemon": poke.name,
    "imagem": poke.img, 
    "altura": (parseFloat(poke.height.match(/[+-]?\d+(?:\.\d+)?/g).join("")))}))
  
  const alturaMedia = alturas.reduce((acum, poke) => poke.altura + acum, 0) / alturas.length
  return { number: alturaMedia, array: alturas } 
}

app = {
  sortData,
  filterData,
  calculaAlturas
}
